The purpose of this project is to build an online multiplayer drifting race game.


specifics:
	functionality
the core functionality of this game will be how the cars control.  I'm thinking initially that either as a function of traction or speed, a car will move between moving as by friction centrifugal force and via spinning with acceleration in the faced direction.

	graphics
I would like to pelt out this game as fast as possible, graphics will be drawn via canvas drawing calls.

	camera
camera will play an important role.  When building the camera, the ability to  translate, rotate, and rescale the camera must be considered.

	controls
players will control with either wasd or arrow keys.  some other key will function as ebrake and another will be boost.  the intricacies of how boost is earned must be considered witha longer thought.

	gameplay
the cars will be driftable but hopefully feel competent.  Players should not be punished for drifting, so likely boost will be handed out as a reward for drifting.
-tracks should in the final product be autgenerated.  I was thinking possibly about a simple system where tracks are started from a circle.  branches are placed and segments elongated.  These would result in randomly generated but probably boring tracks.
-collision I would like there to be oval or rectangular based collisions that would allow for spinning the affected cars during the collision.
-JUMPS. There could be jumps allowing figure 8 configurations.  This opens the door to potential cheating by skipping segments.  a requirement should be added that a player must pass through the furthest area on the track before they can complete a lap.
-ground types.  There should be different ground types, server side, this would allow different frictions and car feel.  Client side, this would result in different particle effects as the car slides around.

	physics.
I need to have a discussion with myself on how the physics should work
I am thinking that there should be an overall affect of friction upon the non car facing velcoity but the velcity along the line of the cars direction should be affected as a relationship between current speed, slippage, and wheel speed. with out any slippage, you would be moving along this axis in perfect accord with wheel speed.
-i have not had the time to consider the implications of this system or whether or not it would be fun.

	cars and their important variables.
position
velocity
rotation
rotationalvelocity //for when a car get spun out
traction?
boost

server->client car variables.
position
rotation
velcoity
boost

client->server
left, right, gas, brake, ebrake, boost


	Server hosting
right now, the plan is to discover how to use an amazon web server.  Matthew cech's suggestion is to run an instance of nginx based on a tutorial provided by digital ocean.  They suggest using some sort of certbot in order to allow for https connection.
I have not actually looked into any of this at all.

bugs:
the canvas is stretching to the size defined in the style guide rather than actually gaining size properly


change logs:
17/08/19: 
lots of copy pasta and reorganization from my first attempt at building a client/server io game. this time is much more readable.

17/08/20: 
most of the server and client have been built.  client is still missing a draw function. this is the last step to beginning to play test the game.  Also much of the physics on the server side has been flubbed simply to push onwards to a testabl state.  added a good deal to thoughts in the readme.