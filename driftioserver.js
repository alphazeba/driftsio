/*
Aron Hommas 2017
this is the main server .js file for driftio.
the server is running using node.js
*/

'use strict';

  ///////////
 //classes//
///////////

//the standard racing unit
class Car{
	//allows you to define the starting position and rotation
	constructor(x,y,r){
		//static info
		//position
		//rotation
		this.pos ={
			x: x,
			y: y
		};
		this.rot = r;
		
		//delta info
		//velocity
		//rotational velcoty
		this.v ={
			x: 0,
			y: 0
		};
		this.rotv = 0;
		
		//traction info
		//wheelSpd
		//can be compared with the velocity magnitude to find slippage amounts.
		this.wheelSpd = 0;
		this.slippage = 0;//at slippage = 0, velocity is equal to wheelspd in rotation direction. at 1, wheelspd is entirely ignored.
		
		this.ebrk = false;
		
		//boost amount
		this.boost = 30;
		
		
		//constants and stuff such as acceleration
		this.turnRate = 0.005;
		this.acceleration = 0.01;
		this.boostMultiplier = 4;
		this.decceleration = 0.8;
		this.wheelDecceleration = 0.99;
		this.rotFriction = 0.92;
		
		
		//collision optimization
		this.vertSpace = 0;
		this.horiSpace = 0;
		this.dcx = 0;
		this.dcx = 0;
		this.calculatedCollisionAngle = 3939;
	}
	
	createClientPacket(){
		return {
			type: 'car',
			pos: this.pos,
			rot: this.rot,
			v: this.v,
			boost: this.boost,
			ebrk: this.ebrk
		};
	}

	update(controls){
		this.ebrk = controls.ebrk;
		//boost charge
		this.boost += 0.05;
		
		//console.log('update is running on player');
		//controller input will not be handled here.
			//going
		if(controls.boost && this.boost > 0){//if boosting, ignore whether gas is pressed.
			this.wheelSpd += this.acceleration * this.boostMultiplier
			this.boost--;
		}
		else if(controls.gas){
			this.wheelSpd += this.acceleration;
		}
		
			//stopping
			/*
		if(controls.ebrk){
			this.wheelSpd = 0;
		} 
		else */
		if(controls.brk){
			this.wheelSpd *= this.decceleration;
		}
		
			//turning
		if(controls.left){
			this.rotv -= this.turnRate;
		}
		if(controls.right){
			this.rotv += this.turnRate;
		}
		
		//calculate slippage.
		this.slippage = this.calculateSlippage();
		
		//get friction
		//this is important because the cars will be able to drive on different ground types.
		let skid = 1;
		if(controls.ebrk){
			skid = 4;
		}
		let friction = 1-((this.getFriction()/skid)/(1+this.slippage));
		
		//update friction
		this.v.x *= friction;
		this.v.y *= friction;
		this.rotv *= this.rotFriction;
		if(Math.abs(this.rotv) < 0.001){
			this.rotv = 0;
		}
		
		this.wheelSpd *= this.wheelDecceleration;
		
		//update direction
		this.rot += this.rotv;
		
		//update velocity
		this.v.x = this.v.x + Math.cos(this.rot)*(this.wheelSpd/skid);//*this.slippage;
		this.v.y = this.v.y + Math.sin(this.rot)*(this.wheelSpd/skid);//*this.slippage;
		
		
		//update placement.
		this.pos.x += this.v.x;
		this.pos.y += this.v.y;
	}

	calculateSlippage(){
		
		
		//this function will compare rotation to velocity direction and wheelspeed with velocity magnitude to find the amount of slipping going on.
		//TODO implement this function lel
		
		let output = 0;
		
		
		//directional slippage
		let vMagnitude = Math.sqrt(this.v.x*this.v.x + this.v.y*this.v.y);
		
		let vNormal = {
			x: 0,
			y: 0
		}
		
		if(vMagnitude > 0){
			vNormal.x = this.v.x/vMagnitude;
			vNormal.y = this.v.y/vMagnitude;
		}
		
		let rotNormal = {
			x: Math.cos(this.rot),
			y: Math.sin(this.rot)
		}
		
		let sumNormal = {
			x: rotNormal.x + vNormal.x,
			y: rotNormal.y + vNormal.y
		}
		
		let sumMagnitude = Math.sqrt(sumNormal.x*sumNormal.x + sumNormal.y*sumNormal.y);
		
		output = sumMagnitude/2;  //sumSquareMagnitude max value = 2, min = 0.  divide by 4 makes this a value between 0 and .5
		//console.log(output);
		
		//maybe add spin slippage later?
		
		//console.log(output);
		
		
		//return 0.5;
		
		//returns a number between 0 and 1.
		return output;
	}
	
	getFriction(){
		//have this get the floor type under the car and return the corresponding number.
		return 0.2;
	}
	
	collideWithBox(box){
		
		//TODO proximity check.
		
		
		//find the center of the car.
		let cx = this.pos.x+this.dcx;
		let cy = this.pos.y+this.dcy;
		
		//car size measurements
		//10+30 long / 2
		let rx = 20;
		
		//20 wide / 2
		let ry = 10;
		
		//some calculation saving stuff.
		if(this.calculatedCollisionAngle != this.rot){
		//calculate new collision stuff.
			
			//will find the vertical and horizontal spacers from the center.
			//will then need to find the actual center of the car.
			//it should be 10units away from the origin of the car.
			
			this.horiSpace = Math.abs(Math.cos(this.rot))*rx + Math.abs(Math.sin(this.rot))*ry;
			this.vertSpace = Math.abs(Math.cos(this.rot+Math.PI/2))*rx + Math.abs(Math.sin(this.rot+Math.PI/2))*ry;
			
			this.dcx = Math.cos(this.rot) * 10;
			this.dcy = Math.sin(this.rot) * 10;
			
			this.calculatedCollisionAngle = this.rot;
		}
		
		let bounce = 0;
		let slow = 0.95;
		
		//collision against a plane.
		if(cy > box.y && cy < box.y + box.h){ //is to the left or right of the box.
			//collision left or right
			//right
			
			let point = cx+this.horiSpace;
			if(point > box.x && point < box.x+box.w ){
				//collision on the right
				this.pos.x -= point - box.x;
				this.v.x *= bounce;
				this.v.y *= slow;
				this.wheelSpd *= slow;
				
			} else {
				point = cx-this.horiSpace;
				if(point > box.x && point < box.x+box.w ){
					//collision on the left
					this.pos.x -= point - (box.x+box.w);
					this.v.x *= bounce;
					this.v.y *= slow;
					this.wheelSpd *= slow;
				}
			}
		}
		else if(cx > box.x && cx < box.x+box.w){  //is above or below the box.
			//collision top or bottom
			//bottom
			let point = cy+this.vertSpace;
			if(point > box.y && point < box.y+box.h){
				//collision on the bototm
				this.pos.y -= point - box.y;
				this.v.y *= bounce;
				this.v.x *= slow;
				this.wheelSpd *= slow;
				
			} else {
				point = cy-this.vertSpace;
				if(point > box.y && point < box.y+box.h){
					//collision above
					this.pos.y -= point - (box.y+box.h);
					this.v.y *= bounce;
					this.v.x *= slow;
					this.wheelSpd *= slow;
				}
			}
		}
		//collision on a corner.
		else{
			let point;
			//find which corner is closest
			if(cx < box.x){  //car to the left.
				if(cy < box.y){ //car above left
					point = {
						x: box.x,
						y: box.y
					};
				} else {        //car below left
					point = {
						x: box.x,
						y: box.y+box.h
					};
				}
			} else {         //car to the right.
				if(cy < box.y){ //car above right
					point = {
						x: box.x+box.w,
						y: box.y
					};
				} else {        //car below right
					point = {
						x: box.x+box.w,
						y: box.y+box.h
					}
				}
			}
			
			//relate the point to the center of the car.
			point.x -= cx;
			point.y -= cy;
			//rotate the point so that it is as if the car where facing 0
			let tx = point.x;
			let ty = point.y;
			point.x = Math.cos(-this.rot)*tx - Math.sin(-this.rot)*ty;
			point.y = Math.sin(-this.rot)*tx + Math.cos(-this.rot)*ty;
			
			//check if collision with point
			if(point.x > -rx && point.x < rx && point.y > -ry && point.y < ry){
				
				
				//there was a corner collision
				//was the collision less horizontal or vertical?
				let diffx = 0;
				let diffy = 0;
				if(Math.abs(point.x) > Math.abs(point.y)){ //horizontal collision
					if(point.x < 0){ //left 
						diffx = -rx-point.x;
					} else {		 //right
						diffx = rx-point.x;
					}
				} else {									//vertical collision
					if(point.y < 0){ //above
						diffy = -ry-point.y;
					} else {			//below
						diffy = ry-point.y;
					}
				}
				
				//now must rerotate diffx and y into real world space so we can push the car off the wall
				tx = diffx;
				ty = diffy;
				diffx = Math.cos(this.rot)*tx - Math.sin(this.rot)*ty;
				diffy = Math.sin(this.rot)*tx + Math.cos(this.rot)*ty;
				
				//now push the car off the wall finally.
				this.pos.x -= diffx;
				this.pos.y -= diffy;
				this.wheelSpd *= slow;
			}
		}
		
		
		//console.log(String(cx) + ' ' + String(cy));
		
		//these collision tests will assume that it is not possible to collide with top AND bottom or left AND rigt.
		
	}
};

//the player class holds both the public info that the client can know and the server only variables.
//this also holds the car state.
class Player{
	constructor(socketid,userip){
		//identifying information
		this.id = socketid;
		this.ip = userip;
		
		//controller information
		this.controls = {
			left: false,
			right: false,
			gas: false,
			brk: false,
			ebrk: false,
			boost: false
		};
		
		this.car = new Car(240,240,-Math.PI/4);
	}
	
	update(){
		this.car.update(this.controls);
	}
	
	passUserInput(data){
		this.controls = data;
	}
	
	getClientPackage(){
		//returns the car's state if the car exists.
		if(this.car){
			return this.car.createClientPacket();
		}
		else {
			return {};
		}
	}
	
};

//the box is a basic collidable rectangle. it cannot be rotated.
class Box{
	constructor(x,y,w,h){
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
	}
	
	getClientPackage(){
		return {
			type: 'box',
			pos: {
				x: this.x,
				y: this.y
			},
			w: this.w,
			h: this.h
		};
	}
};

  /////////////
 //functions//
/////////////
function buildTestTrack(){
	track = {};
	track[0] = new Box(0,0,40,1000);
	track[1] = new Box(40,0,1920,40);
	track[2] = new Box(1960,0,40,690);
	track[3] = new Box(2000,690,960,40);
	track[4] = new Box(2960,690,40,1280);
	track[5] = new Box(1000,2000,1960,40);
	track[6] = new Box(1000,1040,40,960);
	track[7] = new Box(0,1000,1000,40);
	track[8] = new Box(680,360,640,160);
	track[9] = new Box(1640,1040,640,640);
}

function buildTrackVortex(){
	let l = {}
	let i=  0 ;
	l[i] =  {
		x: 1,
		y: 0,
		w: 24,
		h: 1
	};     
	i += 1 ;
	l[i]  = {
		x: 25,
		y: 0,
		w: 1,
		h: 19
	};     
	i += 1 ;
	l[i]  = {
		x: 1,
		y: 18,
		w: 24,
		h: 1
	};     
	i += 1 ;
	l[i]  = {
		x: 0,
		y: 0,
		w: 1,
		h: 19
	};     
	i += 1 ;
	l[i]  = {
		x: 3,
		y: 3,
		w: 1,
		h: 13
	};     
	i += 1 ;
	l[i]  = {
		x: 4,
		y: 3,
		w: 19,
		h: 1
	};     
	i += 1 ;
	l[i]  = {
		x: 22,
		y: 4,
		w: 1,
		h: 6
	};     
	i += 1 ;
	l[i]  = {
		x: 17,
		y: 9,
		w: 5,
		h: 1
	};     
	i += 1 ;
	l[i]  = {
		x: 9,
		y: 4,
		w: 1,
		h: 11
	};     
	i += 1 ;
	l[i]  = {
		x: 9,
		y: 15,
		w: 12,
		h: 1
	};     
	i += 1 ;
	l[i]  = {
		x: 6,
		y: 6,
		w: 1,
		h: 12
	};     
	i += 1 ;
	l[i]  = {
		x: 12,
		y: 6,
		w: 1,
		h: 7
	};     
	i += 1;
	l[i] = {
		x: 13,
		y: 6,
		w: 5,
		h: 1
	};
	
	i += 1 ;
	l[i]  = {
		x: 13,
		y: 12,
		w: 13,
		h: 1
	};
	i += 1;
	console.log('track is building');
	buildGridTrack(l,120,120);
}

function buildGridTrack(l,w,h){
	track = {};
	for(var i in l){
		console.log(String(l[i].x) + ',' + String(l[i].y));
		track[i] = new Box(l[i].x*w,l[i].y*h,l[i].w*w,l[i].h*h);
	}
	
	console.log(String(i) + ' blocks were built.');
}

  ////////////////
 //requirements//
////////////////
var express = require('express');
var http = require('http');
var path = require('path');
var socketIO = require('socket.io');

  ////////////////
 //server setup//
////////////////
var app = express();
var server = http.Server(app);
var io = socketIO(server);

app.set('port',5000); //uses port 5000 maybe should change this?
app.use('/',express.static(__dirname + '/client'));

//routing
app.get('/',function(request, response){
	response.sendFile(path.join(__dirname, 'index.html'));
});

//starts the server
server.listen(5000, function(){
	console.log('starting the server on port 5000');
});

  ///////////////////
 //socket handlers//
///////////////////
io.on('connection' , function(socket){
	//add a player
	
	
	socket.on('new player', function(){
		
		//check that it is a valid player
		let socketip = socket.handshake.address;
		let similarIP = 0;
		for(var i in players){
			if(players[i].ip === socketip){
				similarIP++;
			}
		}
		
		if(similarIP<6){ //this allows for 5 people on the same network to play.
			console.log('new player on socket ' + String(socket.id) + 'with ip: ' + String(socketip));
		
			//add a new player to the game.
			players[socket.id] = new Player(socket.id,socketip);
			
			
			//emit a 'you' message to the connected client. to provide them with their id.
			socket.emit('you',socket.id);
			
			//TODO announce to the clients the new arrival
		} else {
			socket.disconnect();
		}
		
	});
	
	//remove a player
	socket.on('disconnect', function(){
		console.log('player on socket ' + String(socket.id) + ' disconnected');
		
		
		//remove the player from the game.
		delete players[socket.id];
		
		//TODO announce to the clients that a player has disconnected.
	});
	
	//recieve a client update
	socket.on('clientUpdate', function(data){
		if(players[socket.id]){ //if the player exists.
			players[socket.id].controls = data;
		} else {
			console.log('recieving updates from a ghost player.');
			socket.disconnect();
		}
	});
	
	//recieve a message
	socket.on('message', function(data){
		//TODO messages recieved from a client should be broadcast to all the clients.
		console.log('recieved message from ' + String(socket.id) + ': ' + data);
	});
});

  //////////////
 //game setup//
//////////////
var players = {};

var track = {};
//buildTestTrack();
buildTrackVortex();

  ///////////////////////
 //serverside gameloop//
///////////////////////
setInterval(function(){
	
	//io.sockets.emit('message', 'hello, this is your server');
	
	//update the server and build the state to be sent to clients.
	let state = {};
	
	for(var i in players){
		players[i].update();
		for(var j in track){
			players[i].car.collideWithBox(track[j]);
		}
		state[i] = players[i].getClientPackage();
	}
	
	
	//adds the track segments to the state.
	for(var i in track){
		state[i] = track[i].getClientPackage();
	}
	
	//informs clients of the game state.
	io.sockets.emit('stateUpdate',state);
},1000/60);//TODO change the interval to something much smaller when finished testing the connections.


