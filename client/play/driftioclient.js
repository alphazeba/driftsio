//this will be the driftio client//

  ///////////
 //classes//
///////////
class Camera{
	constructor(x,y,z,r){
		this.pos = {
			x: x,
			y: y
		}
		
		this.zoom = z;
		this.rot = r;
		this.drawables = {};
	}
	
	draw(drawPacket){
		//check whether the object would be in the field of view.
		//if so, add the item to the list of drawables.
		//also, math concerning the drawing packet should be called here.
		if(true){ //TODO change this so that it checks whether or not the object will be close to being in the field of view.
		
		
			//translate objects so that 0,0 is exactly where the camera is.
			drawPacket.x -= this.pos.x;
			drawPacket.y -= this.pos.y;
			
			//rotate everything so that it is relative to the camera.
				//rotation
			drawPacket.rot -= this.rot; //easy part lel
				//coordinates.
			let tx = drawPacket.x;
			let ty = drawPacket.y;
			drawPacket.x = tx * Math.cos(-this.rot) - ty * Math.sin(-this.rot);
			drawPacket.y = tx * Math.sin(-this.rot) + ty * Math.cos(-this.rot);
				
			//zoom and what not.
			drawPacket.scale *= this.zoom;
			drawPacket.x *= this.zoom;
			drawPacket.y *= this.zoom;
			
			//now relate everything to 2d screen space where 0,0 is in the top left corner
			drawPacket.x += canvas.width/2;
			drawPacket.y += canvas.height/2;
			
			//add the drawpacket to the drawables list.
			this.drawables[drawPacket.id] = drawPacket;
		} else {
			//if not adding the drawPacket to the drawlist, what do?
		}
		
		//drawing packets should have id,x,y,scale,rotation,drawCallback(x,y,scale,rotation,context)
	}
	
	render(){
		//this is where everything will be drawn.
		//draw background.
		context.fillStyle = 'green';
		context.fillRect(0,0,canvas.width,canvas.height);
		
		//
		particleHandler.draw();
		
		//context and canvas are global variables and so do not need to be passed.
		for(var i in this.drawables){
			let drawable = this.drawables[i];
				switch(drawable.type){
				case 'car':
				case 'prt':
					drawable.drawCallback(drawable.x,drawable.y,drawable.scale,drawable.rot);
				break;
				
				case 'box':
					drawable.drawCallback(drawable.x,drawable.y,drawable.w,drawable.h,drawable.scale,this.rot);
				break;
			}
			
		}
		
		//clear the list now that everything has been drawn.
		this.drawables = {};
	}

	//absolute placement.
	set(x,y,z,r){
		this.pos.x = x;
		this.pos.y = y;
		this.zoom = z;
		this.rot = r;
	}
	
	//relative movement
	move(x,y,z,r){
		this.pos.x += x;
		this.pos.y += y;
		this.zoom += z;
		this.rot += rot;
	}
};

class Particle{
	constructor(x,y){
		this.x = x;
		this.y = y;
		this.lifeSpan = 300;
	}
	
	getDrawPacket(tid){
		this.lifeSpan = Math.max(0,this.lifeSpan-1);
		return {
			type: 'prt',
			id: tid+500,
			x: this.x,
			y: this.y,
			scale: this.lifeSpan/300,
			rot: 0,
			drawCallback: drawParticle
		};
	}
}

class ParticleHandler{
	
	//TODODOOODODO
	//for some reason only a single particle ever gets drwan.
	constructor(max){
		this.particles = [];
		this.maxParticles = max;
		this.curParticle = 0;
	}
	
	makeParticle(x,y){
		this.particles[this.curParticle] = new Particle(x,y);
		this.curParticle++;
		if(this.curParticle>this.maxParticles){ //TODO figure out how mod works in javascript.
			this.curParticle = 0;
		}
	}

	draw(){
		let ran  = 0;
		for(var i in this.particles){
			ran += 1;
			camera.draw(this.particles[i].getDrawPacket(i));
		}
	}
}

  /////////////
 //functions//
/////////////

function drawCar(x,y,scale,rotation){ //TODO make the car prettier.
	context.strokeStyle = 'black';
	context.lineWidth = 20*scale;
	let carNormalx = Math.cos(rotation);
	let carNormaly = Math.sin(rotation);
	
	context.beginPath();
	context.moveTo(x+carNormalx*scale*(-10),y+carNormaly*scale*(-10));
	context.lineTo(x+carNormalx*scale*30,y+carNormaly*scale*30);
	context.stroke();
	
	context.strokeStyle = 'white';
	context.lineWidth = 4*scale;
	
	context.beginPath();
	context.moveTo(x+carNormalx*scale*(-10),y+carNormaly*scale*(-10));
	context.lineTo(x+carNormalx*scale*30,y+carNormaly*scale*30);
	context.stroke();
	
	context.strokeStyle = 'gray';
	context.lineWidth = 16*scale;
	context.beginPath();
	context.moveTo(x,y);
	context.lineTo(x+carNormalx*scale*(-3),y+carNormaly*scale*(-3));
	context.moveTo(x+carNormalx*scale*(12),y+carNormaly*scale*(12));
	context.lineTo(x+carNormalx*scale*(17),y+carNormaly*scale*(17));
	context.stroke();
	
	
}

function drawParticle(x,y,scale,rotation){
	context.fillStyle = 'black';
	
	context.beginPath();
	context.arc(x,y,3*scale,0,2*Math.PI);
	context.fill();
}

function drawBox(x,y,w,h,scale,cRot){
	context.strokeStyle = 'black';
	context.lineWidth = h*scale;
	
	let normalx = Math.cos(-cRot);
	let normaly = Math.sin(-cRot);
	let downNormalx = Math.cos(Math.PI/2-cRot);
	let downNormaly = Math.sin(Math.PI/2-cRot);
	
	context.beginPath();
	context.moveTo(x+downNormalx*h/2*scale,y+downNormaly*h/2*scale);
	context.lineTo(x+(downNormalx*h/2+normalx*w)*scale,y+(downNormaly*h/2+normaly*w)*scale);
	context.stroke();
}

function drawBoost(boost, h,max,maxW,color){
	
	let w = boost/max * maxW;
	context.strokeStyle = color;
	context.lineWidth = h;
	context.beginPath();
	context.moveTo(canvas.width/2-w,canvas.height-h/2);
	context.lineTo(canvas.width/2+w,canvas.height-h/2);
	context.stroke();
}
//setup the connection
var socket = io();

  ////////////////////
 //server -> client//
////////////////////
socket.on('message',function(data){
	console.log(data);
});

socket.on('stateUpdate',function(data){
	//TODO i am getting fairly tired. right here i need to draw the game state 
	
	for(var i in data){
		cur = data[i];
		if(cur){ //if it exists
			switch(cur.type){
				case 'car':
					if(i == selfid){
						ego = data[i];
					}
					
					//create particles
					//make particles
					if(cur.ebrk){
						let rightx = Math.cos(cur.rot+Math.PI/2)*8;
						let righty = Math.sin(cur.rot+Math.PI/2)*8;
						particleHandler.makeParticle(cur.pos.x+rightx,cur.pos.y+righty);
						particleHandler.makeParticle(cur.pos.x-rightx,cur.pos.y-righty);
					
					}
					
					//id,x,y,scale,rotation,drawCallback(x,y,scale,rotation)
					drawPacket = {
						type: 'car',
						id: i,
						x: cur.pos.x,
						y: cur.pos.y,
						scale: 1,
						rot: cur.rot,
						drawCallback: drawCar 
					}
					//console.log('x:'+String(drawPacket.x) + ' y:' + String(drawPacket.y) + ' rot:' + String(drawPacket.rot));
					camera.draw(drawPacket);
				break;
				
				case 'box':
					drawPacket = {
						type: 'box',
						id: i,
						x: cur.pos.x,
						y: cur.pos.y,
						w: cur.w,
						h: cur.h,
						scale: 1,
						drawCallback: drawBox
					}
					
					camera.draw(drawPacket);
				break;
				
				default:
					console.log('something other than a car or a box was passed in the state update');
			}
			
		}
		else {
			console.log('for some reason, an empty object was passed in the data packet');
		}
	}
	
	
	//once everything is done.
	camera.render();
	
	if(ego != null){ // TODO TODO TODO this is brekaing the game at the moment.
		
		camera.set(( (ego.pos.x+ego.v.x*30) ),( (ego.pos.y+ego.v.y*30)) ,1/(1+Math.sqrt(ego.v.x*ego.v.x+ego.v.y*ego.v.y)/10),(camera.rot*9 + ego.rot+Math.PI/2)/10);
		drawBoost(ego.boost,15,400,400,'#f44242');
	}
});

socket.on('you',function(data){
	selfid = data;
});

  ////////////////////
 //client -> server//
////////////////////

//announce the presence of a new player.
socket.emit('new player');

//setup the main communication loop.
 setInterval(function() {
	//socket.emit('movement',movement);
	//socket.emit('message', 'hello this is a client');
	
	socket.emit('clientUpdate',controls);
 }, 1000/60); //TODO change the interval to somethign much smaller later.

 
  ////////////////////////
 //setup game variables//
////////////////////////
var canvas = document.getElementById('gameScreen');
var context = canvas.getContext('2d');

var selfid = 98987;
var ego;

var controls = {
	left: false,
	right: false,
	gas: false,
	brk: false,
	ebrk: false,
	boost: false
};
 
var camera = new Camera(0,0,1,0);
var particleHandler = new ParticleHandler(300);
 
  /////////////////////////
 //setup event listeners//
/////////////////////////
 document.addEventListener('keydown', function(event) {
	switch(event.keyCode){
		case 65: //a
			controls.left = true;
			break;
		case 87: //w
			controls.gas = true;
			break;
		case 83: // s
			controls.brk = true;
			break;
		case 68: // d 
			controls.right = true;
			break;
		case 32: // space
			controls.ebrk = true;
			break;
		case 16: //enter
			controls.boost = true;
			break;
		default:
			//idk if anything should be done if a differnet key is pressed.
	}
 });
 
 document.addEventListener('keyup', function(event) {
	switch(event.keyCode){
		case 65: //a
			controls.left = false;
			break;
		case 87: //w
			controls.gas = false;
			break;
		case 83: // s
			controls.brk = false;
			break;
		case 68: // d 
			controls.right = false;
			break;
		case 32: // space
			controls.ebrk = false;
			break;
		case 16: //enter
			controls.boost = false;
			break;
		default:
			//idk if anything should be done if a differnet key is pressed.
	}
 });